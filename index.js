const app = require("http").createServer(handler)
const io = require("socket.io")(app)
var fs = require("fs")

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Erro para carregar o index.html' + err)
            }
          res.writeHead(200);
          res.end(data)  
        })
}
app.listen(3001);
console.log('Wellcome!! Open the browser and access this address: http://localhost:3001')

var allClients = 0;
io.sockets.on('connection', function(socket) {
   allClients++

   socket.on('disconnect', function() {
      allClients--
      console.log('Clientes conectados = ' + allClients);
   });

   if (allClients > 0) {
        console.log('Clientes conectados = ' + allClients);
    }
})
